FROM ubuntu:latest

RUN apt-get update && \
  apt-get install -y \
  ca-certificates \
  rbenv \
  curl \
  git \
  libssl-dev \
  libreadline-dev \
  zlib1g-dev \
  libyaml-dev \
  build-essential \
  autoconf \
  bzip2 \
  build-essential \
  ruby-dev && \
  apt-get clean

RUN mkdir -p "$(rbenv root)"/plugins && \
  git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

RUN rbenv install 2.4.2

RUN rbenv local 2.4.2 && \
  eval "$(rbenv init -)" && \
  gem install bundler

COPY .irbrc /root/.irbrc

COPY ruby.sh ruby.sh
RUN chmod ugo+x ruby.sh
ENTRYPOINT ["/ruby.sh"]
CMD ["ruby","--version"]

