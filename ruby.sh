#!/bin/bash

eval "$(rbenv init -)"

if [ -n "$GEMS" ]; then
  gem install $GEMS
fi

exec $@

