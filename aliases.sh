alias ruby='MSYS_NO_PATHCONV=1 docker run --rm -e GEMS=$GEMS -v $PWD:/workspace -w /workspace --network grid registry.gitlab.com/comiq/ruby-container:snapshot ruby'
alias irb='MSYS_NO_PATHCONV=1 docker run --rm -e GEMS=$GEMS -ti -v $PWD:/workspace -w /workspace --network grid registry.gitlab.com/comiq/ruby-container:snapshot irb'
alias rubyshell='MSYS_NO_PATHCONV=1 docker run --rm -e GEMS=$GEMS -ti -v $PWD:/workspace -w /workspace --network grid registry.gitlab.com/comiq/ruby-container:snapshot irb'
